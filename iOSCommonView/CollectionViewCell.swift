//
//  CollectionViewCell.swift
//  BonnieDraw
//
//  Created by Professor on 29/12/2017.
//  Copyright © 2017 Professor. All rights reserved.
//

import UIKit

open class CollectionViewCell: UICollectionViewCell {
    @IBInspectable var highlightedColor: UIColor = .groupTableViewBackground
    override open var isHighlighted: Bool {
        willSet {
            if newValue {
                backgroundColor = highlightedColor
            }
        }
        didSet {
            if !oldValue {
                UIView.animate(withDuration: 0.3) {
                    self.backgroundColor = nil
                }
            }
        }
    }
}
