//
//  SecureTextField.swift
//  NucliasConnect
//
//  Created by Professor on 2019/4/22.
//  Copyright © 2019 Professor. All rights reserved.
//

import UIKit

open class SecureTextField: UITextField {
    @IBInspectable var enabledSecureTextIcon: String = "" {
        didSet {
            if button.isSelected {
                button.setImage(UIImage(named: enabledSecureTextIcon), for: .normal)
            }
        }
    }
    @IBInspectable var disabledSecureTextIcon: String = "" {
        didSet {
            if !button.isSelected {
                button.setImage(UIImage(named: disabledSecureTextIcon), for: .normal)
            }
        }
    }
    private let button = UIButton(type: .custom)

    open override func awakeFromNib() {
        button.isSelected = isSecureTextEntry
        button.setImage(UIImage(named: button.isSelected ? enabledSecureTextIcon : disabledSecureTextIcon), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(toggleSecureText), for: .touchUpInside)
        if button.isSelected {
            NotificationCenter.default.removeObserver(self)
        } else {
            NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        }
        rightView = button
        rightViewMode = .always
    }

    @IBAction func toggleSecureText(_ sender: Any) {
        button.isSelected = !isSecureTextEntry
        button.setImage(UIImage(named: button.isSelected ? enabledSecureTextIcon : disabledSecureTextIcon), for: .normal)
        isSecureTextEntry = button.isSelected
        let text = self.text
        self.text = nil
        self.text = text
        if button.isSelected {
            NotificationCenter.default.removeObserver(self)
        } else {
            NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        }
    }

    @objc func applicationWillEnterForeground(notification: Notification) {
        isSecureTextEntry = button.isSelected
        let text = self.text
        self.text = nil
        self.text = text
    }

    @objc func applicationDidEnterBackground(notification: Notification) {
        isSecureTextEntry = true
        let text = self.text
        self.text = nil
        self.text = text
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
