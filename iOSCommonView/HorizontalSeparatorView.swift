//
//  HorizontalSeparatorView.swift
//  iOSTemplate
//
//  Created by Professor on 2021/3/4.
//  Copyright © 2021 Agrowood. All rights reserved.
//

import UIKit

open class HorizontalSeparatorView: UIView {
    private var color: UIColor?

    open override func awakeFromNib() {
        color = backgroundColor
        backgroundColor = .clear
    }

    open override func draw(_ rect: CGRect) {
        color?.setFill()
        UIBezierPath(rect: rect.inset(by: UIEdgeInsets(top: rect.height / 4, left: 0, bottom: rect.height / 4, right: 0))).fill()
    }
}
