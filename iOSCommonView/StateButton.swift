//
//  StateButton.swift
//  iOSTemplate
//
//  Created by Professor on 2018/6/10.
//  Copyright © 2018年 Agrowood. All rights reserved.
//

import UIKit
import iOSUtility

open class StateButton: UIButton {
    private var normalColor: UIColor?
    @IBInspectable var highlightedColor: UIColor?
    @IBInspectable var selectedColor: UIColor?
    @IBInspectable var disabledColor: UIColor?
    override open var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                if isEnabled {
                    backgroundColor = highlightedColor
                } else {
                    backgroundColor = disabledColor
                }
            } else {
                UIView.animate(withDuration: 0.3) {
                    self.backgroundColor = self.normalColor
                }
            }
        }
    }
    override open var isSelected: Bool {
        didSet {
            if isSelected {
                if isEnabled {
                    backgroundColor = selectedColor
                } else {
                    backgroundColor = disabledColor
                }
            } else {
                UIView.animate(withDuration: 0.3) {
                    self.backgroundColor = self.normalColor
                }
            }
        }
    }
    override open var isEnabled: Bool {
        didSet {
            backgroundColor = isEnabled ? normalColor : disabledColor
        }
    }

    open override func awakeFromNib() {
        normalColor = backgroundColor
        let accentColor = backgroundColor?.darker(by: 20)
        if highlightedColor == nil {
            highlightedColor = accentColor
        }
        if selectedColor == nil {
            selectedColor = accentColor
        }
        if disabledColor == nil {
            disabledColor = backgroundColor?.lighter(by: 20)
        }
        backgroundColor = isEnabled ? normalColor : disabledColor
    }

    public func setColor(color: UIColor?) {
        normalColor = color
        let accentColor = color?.darker(by: 20)
        highlightedColor = accentColor
        selectedColor = accentColor
        disabledColor = color?.lighter(by: 20)
        backgroundColor = isEnabled ? normalColor : disabledColor
    }
}
