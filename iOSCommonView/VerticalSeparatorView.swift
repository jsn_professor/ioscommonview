//
//  VerticalSeparatorView.swift
//  iOSTemplate
//
//  Created by Professor on 2021/3/4.
//  Copyright © 2021 Agrowood. All rights reserved.
//

import UIKit

open class VerticalSeparatorView: UIView {
    private var color: UIColor?

    open override func awakeFromNib() {
        color = backgroundColor
        backgroundColor = .clear
    }

    open override func draw(_ rect: CGRect) {
        color?.setFill()
        UIBezierPath(rect: rect.inset(by: UIEdgeInsets(top: 0, left: rect.width / 4, bottom: 0, right: rect.width / 4))).fill()
    }
}
