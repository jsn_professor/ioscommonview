//
//  TableViewCell.swift
//  BonnieDraw
//
//  Created by Professor on 29/12/2017.
//  Copyright © 2017 Professor. All rights reserved.
//

import UIKit

open class TableViewCell: UITableViewCell {
    @IBInspectable var highlightedColor: UIColor = .groupTableViewBackground

    open override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            backgroundColor = highlightedColor
        } else {
            UIView.animate(withDuration: 0.3) {
                self.backgroundColor = nil
            }
        }
    }
}
