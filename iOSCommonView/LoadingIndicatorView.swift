//
//  LoadingIndicatorView.swift
//  Omna
//
//  Created by Jason Hsu 08329 on 2/17/17.
//  Copyright © 2017 D-Link. All rights reserved.
//

import UIKit
import iOSUtility

open class LoadingIndicatorView: UIView {
    override open var isHidden: Bool {
        didSet {
            if isHidden {
                indicator.stopAnimating()
            } else {
                indicator.startAnimating()
            }
        }
    }
    public var indicator = UIActivityIndicatorView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addIndicator()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addIndicator()
    }

    private func addIndicator() {
        indicator.style = .whiteLarge
        addAndCenter(subView: indicator)
    }

    public func hide(_ hide: Bool) {
        if hide {
            UIView.animate(withDuration: 0.3, animations: {
                self.alpha = 0
            }) {
                finished in
                self.isHidden = hide
            }
        } else {
            isHidden = hide
            UIView.animate(withDuration: 0.3) {
                self.alpha = 1
            }
        }
    }
}
