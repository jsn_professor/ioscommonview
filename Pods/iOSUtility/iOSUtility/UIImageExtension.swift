//
//  UIImageExtension.swift
//  iOSTemplate
//
//  Created by Professor on 08/11/2017.
//  Copyright © 2017 Agrowood. All rights reserved.
//

import UIKit

extension UIImage {
    public func color(atPoint point: CGPoint) -> UIColor? {
        guard let cgImage = cgImage, let pixelData = cgImage.dataProvider?.data else {
            return nil
        }
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        let pixelInfo: Int = ((cgImage.bytesPerRow * Int(point.y)) + (Int(point.x) * cgImage.bitsPerPixel / 8))
        return UIColor(red: CGFloat(data[pixelInfo + 2]) / 255,
                green: CGFloat(data[pixelInfo + 1]) / 255,
                blue: CGFloat(data[pixelInfo]) / 255,
                alpha: CGFloat(data[pixelInfo + 3]) / 255)
    }
}
