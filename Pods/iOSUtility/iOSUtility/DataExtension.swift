//
//  DataExtension.swift
//  Omna
//
//  Created by Jason Hsu 08329 on 2016/12/1.
//  Copyright © 2016年 D-Link. All rights reserved.
//

import Foundation

extension Data {
    mutating public func append(string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }

    public func toAsciiString() -> String? {
        return String(data: self, encoding: String.Encoding.ascii)
    }
}
