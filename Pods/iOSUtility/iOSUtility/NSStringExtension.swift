//
//  LocalizedString.swift
//  Omna
//
//  Created by Jason Hsu 08329 on 10/13/16.
//  Copyright © 2016 D-Link. All rights reserved.
//

import UIKit

extension NSString {
    public func fontSize(rect: CGRect, maxSize: CGFloat, minSize: CGFloat = 1) -> CGFloat {
        var fontSize = maxSize
        while size(withAttributes: [.font: UIFont.systemFont(ofSize: fontSize)]).width > rect.width {
            fontSize -= 0.1
            if fontSize < minSize {
                return minSize
            }
        }
        return fontSize
    }
}
