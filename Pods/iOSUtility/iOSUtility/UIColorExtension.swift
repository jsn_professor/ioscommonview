//
//  ColorExtension.swift
//  Native
//
//  Created by Jason Hsu 08329 on 1/11/17.
//  Copyright © 2017 D-LINK. All rights reserved.
//

import UIKit

extension UIColor {
    public static var defaultTint: UIColor {
        return UIColor(red: 0.00, green: 0.48, blue: 1.00, alpha: 1.0)
    }

    public func lighter(by percentage: CGFloat = 0) -> UIColor? {
        return self.adjust(by: abs(percentage))
    }

    public func darker(by percentage: CGFloat = 0) -> UIColor? {
        return self.adjust(by: -1 * abs(percentage))
    }

    public func adjust(by percentage: CGFloat = 0) -> UIColor? {
        var r: CGFloat = 0, g: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0;
        if self.getRed(&r, green: &g, blue: &b, alpha: &a) {
            return UIColor(red: min(r + percentage / 100, 1.0),
                    green: min(g + percentage / 100, 1.0),
                    blue: min(b + percentage / 100, 1.0),
                    alpha: a)
        } else {
            return nil
        }
    }
}
