//
//  LabelExtension.swift
//  Omna
//
//  Created by Professor on 21/07/2017.
//  Copyright © 2017 D-Link. All rights reserved.
//

import UIKit

extension UILabel {
    public func setText(text: String?) {
        if text != nil {
            alpha = 0
            UIView.animate(withDuration: 0.3) {
                self.alpha = 1
            }
        }
        self.text = text
    }
}
